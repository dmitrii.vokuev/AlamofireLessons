//
//  UserProfile.swift
//  Networking
//
//  Created by dmitrii on 29.07.19.
//  Copyright © 2019 Alexey Efimov. All rights reserved.
//

import Foundation


struct UserProfile {
    let id: Int?
    let name: String?
    let email: String?
    
    init(data: [String: Any]) {
        let id = data["id"] as? Int
        let name = data["name"] as? String
        let email = data["email"] as? String
        
        self.id = id
        self.email = email
        self.name = name
    }
}
