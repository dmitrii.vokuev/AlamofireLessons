//
//  UserProfileVC.swift
//  Networking
//
//  Created by dmitrii on 24.07.19.
//  Copyright © 2019 Alexey Efimov. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase

class UserProfileVC: UIViewController {
    
    @IBOutlet weak var userNameLabel: UILabel!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    lazy var fbLoginButton: UIButton = {
        let login = FBLoginButton()
        login.frame = CGRect(x: 32, y: view.frame.height - 172, width: view.frame.width - 64, height: 50)
        login.delegate = self
        return login
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addVerticalGradientLayer(topColor: primaryColor, bottomColor: secondaryColor)
        userNameLabel.isHidden = true
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchingUserData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    func setupUI() {
        view.addSubview(fbLoginButton)
    }
}

extension UserProfileVC: LoginButtonDelegate {
    func loginButton(_ loginButton: FBLoginButton!, didCompleteWith result: LoginManagerLoginResult!, error: Error!) {
        
        if error != nil {
            print(error)
            return
        }
        
        guard AccessToken.isCurrentAccessTokenActive else { return }
        
        
        print("Successfully logged in with facebook...")
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton!) {
        print("Did log out of facebook")
        openLoginViewController()
        
    }
    
    private func openLoginViewController() {
        
        do {
            try Auth.auth().signOut()
            
            DispatchQueue.main.async {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let loginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.present(loginViewController, animated: true)
                return
            }
        } catch let error {
            print("Failed to sign out with error: ", error.localizedDescription)
        }
    }
    
    private func fetchingUserData() {
        if Auth.auth().currentUser != nil {
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            Database.database().reference()
                .child("users")
                .child(uid).observeSingleEvent(of: .value, with: ({ (snapshot) in
                    
                    guard let userDara = snapshot.value as? [String: Any] else { return }
                    let currentUser = CurrentUser(uid: uid, data: userDara)
                    self.activityIndicator.stopAnimating()
                    self.userNameLabel.isHidden = false
                    self.userNameLabel.text = "\(currentUser?.name ?? "Noname") logged in with Facebook"
                    
                })) { (error) in
                    print(error)
            }
            
        }
    }
}
