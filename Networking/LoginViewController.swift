//
//  LoginViewController.swift
//  Networking
//
//  Created by dmitrii on 24.07.19.
//  Copyright © 2019 Alexey Efimov. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn

class LoginViewController: UIViewController {
    
    var userProfile: UserProfile?
    
    lazy var fbLoginButton: UIButton = {
        let login = FBLoginButton()
        login.frame = CGRect(x: 32, y: 360, width: view.frame.width - 64, height: 50)
        login.delegate = self
        return login
    }()
    
    lazy var customFBLoginButton: UIButton = {
        let loginButton = UIButton()
        loginButton.backgroundColor = UIColor(hexValue: "#3B5999", alpha: 1)
        loginButton.setTitle("Login with Facebook", for: .normal)
        loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.frame = CGRect(x: 32, y: 440, width: view.frame.width - 64, height: 50)
        loginButton.layer.cornerRadius = 4
        loginButton.addTarget(self, action: #selector(handleCustomFBLogin), for: .touchUpInside)
        return loginButton
    }()
    
    
    lazy var customGoogleLoginButton: GIDSignInButton = {
        let loginButton = GIDSignInButton()
        loginButton.frame = CGRect(x: 32, y: 360 + 80 + 80, width: view.frame.width - 64, height: 50)
        return loginButton
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        view.addVerticalGradientLayer(topColor: primaryColor, bottomColor: secondaryColor)
        GIDSignIn.sharedInstance().delegate = self
        setUpViews()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    private func setUpViews() {
        view.addSubview(fbLoginButton)
        view.addSubview(customFBLoginButton)
        view.addSubview(customGoogleLoginButton)
    }
}


// Mark: FBSDK methods

extension LoginViewController: LoginButtonDelegate {
    
    private func openMainViewController() {
        dismiss(animated: true)
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        if error != nil {
            print(error)
            return
        }
        
        guard AccessToken.isCurrentAccessTokenActive else { return }
        print("Successfully logged in with facebook...")
        signIntoFireBase()
        
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("Did log out of facebook")
    }
    
    @objc private func handleCustomFBLogin() {
        LoginManager().logIn(permissions: ["email", "public_profile"], from: self) { (result, error) in
            
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let result = result else { return }
            
            if result.isCancelled { return }
            else {
                self.signIntoFireBase()
            }
        }
    }
    
    
    private func signIntoFireBase() {
        
        let accsessToken = AccessToken.current
        
        guard let accsessTokenSting = accsessToken?.tokenString else { return }
        
        
        let credentials = FacebookAuthProvider.credential(withAccessToken: accsessTokenSting)
        
        Auth.auth().signIn(with: credentials) { (user, error) in
            if let error = error {
                print("something went wrong with our facebook user" , error)
                return
            }
            
            print("Successfully logged in with our FB user: ")
            self.fetchFacebookFields()
        }
        
    }
    
    private func fetchFacebookFields() {
        
        GraphRequest(graphPath: "me", parameters: ["fields" : "id, name, email"]).start { (_, result, error) in
            if let error = error {
                print(error)
                return
            }
            
            if let userData = result as? [String: Any] {
                self.userProfile = UserProfile(data: userData)
                print(userData)
                print(self.userProfile?.name ?? "nil")
                self.saveIntoFirebase()
            }
            
        }
    }
    
    
    private func saveIntoFirebase() {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let userData = ["name": userProfile?.name, "email": userProfile?.email]
        
        let values = [uid : userData]
        
        Database.database().reference().child("users").updateChildValues(values) { (error, _) in
            if let error = error {
                print(error)
                return
            }
            print("Successfully saved user into firebase database")
            self.openMainViewController()
        }
    }
}

// MARK: GOOGLE sign in

extension LoginViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("Failed to log into Google: ", error)
            return
        }
        
        print("Successfully logged in with our Google user")
        
        guard let authentication = user.authentication else { return }
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (user, error) in
            
            if let error = error {
                print("Something went wrong with our Google user: ", error)
                return
            }
            
            print("Successfully logged into Firebase with Google")
            self.openMainViewController()
        }
    }
}
